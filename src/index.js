const express = require('express');
const dotenv = require('dotenv');
dotenv.config();

const app = express();

const port = process.env.PORT || 8080;

app.get('/', (req, res) => {
    res.send('Hello World!');
});

app.listen(port, (err)=>{
    if(err)console.log("Error : ",err);
    console.log(`Server is running on port ${port}`);
})